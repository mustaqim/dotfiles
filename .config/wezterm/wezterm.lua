-- -*- eval: (rainbow-mode) -*-
local wezterm = require 'wezterm'
local mux = wezterm.mux

local config = wezterm.config_builder()

config.enable_kitty_keyboard = true

config.font = wezterm.font_with_fallback {
  { family = 'Iosevka' },
  'Font Awesome 6 Pro',
  'Font Awesome 6 Brands',
}

-- line_height = 0.2

config.window_decorations = "RESIZE"
-- config.integrated_title_button_style = "Gnome"

config.hide_tab_bar_if_only_one_tab = true
config.use_fancy_tab_bar = false
config.tab_bar_at_bottom = true

config.pane_focus_follows_mouse = true

config.window_padding = {
  left   = '2cell',
  right  = '1cell',
  top    = '0.8cell',
  bottom = '0.2cell',
}

-- config.leader = { key = 'k', mods = 'ALT', timeout_milliseconds = 1000 }

local act = wezterm.action

config.keys = {
  { key = 'i', mods = 'ALT', action = act.SplitVertical { domain = 'CurrentPaneDomain' } },
  { key = 'o', mods = 'ALT', action = act.SplitHorizontal { domain = 'CurrentPaneDomain' } },

  { key = 'q', mods = 'ALT|CTRL', action = act.PaneSelect },

  { key = 'k', mods = 'ALT', action = act.ActivatePaneDirection 'Up' },
  { key = 'j', mods = 'ALT', action = act.ActivatePaneDirection 'Down' },
  { key = 'h', mods = 'ALT', action = act.ActivatePaneDirection 'Left' },
  { key = 'l', mods = 'ALT', action = act.ActivatePaneDirection 'Right' },

  { key = '1', mods = 'ALT', action = act.ActivateTab(0) },
  { key = '2', mods = 'ALT', action = act.ActivateTab(1) },
  { key = '3', mods = 'ALT', action = act.ActivateTab(2) },
  { key = '4', mods = 'ALT', action = act.ActivateTab(3) },
  { key = '5', mods = 'ALT', action = act.ActivateTab(4) },
  { key = '6', mods = 'ALT', action = act.ActivateTab(5) },
  { key = '7', mods = 'ALT', action = act.ActivateTab(6) },
  { key = '8', mods = 'ALT', action = act.ActivateTab(7) },
  { key = '9', mods = 'ALT', action = act.ActivateTab(8) },

}

-- wezterm.on('update-right-status', function(window, pane)
--   local compose = window:composition_status()
--   if compose then
--     compose = 'COMPOSING: ' .. compose
--   end
--   window:set_right_status(compose)
-- end)

config.color_scheme = 'Tomorrow Night'
config.colors = {
  compose_cursor = 'orange',
  scrollbar_thumb = '#373b41',
  ansi = {
    '#1d1f21',
    '#cc6666',
    '#b5bd68',
    '#f0c674',
    '#81a2be',
    '#b294bb',
    '#8abeb7',
    '#c5c8c6'
  },
  brights = {
    '#969896',
    '#d54e53',
    '#b9ca4a',
    '#e7c547',
    '#7aa6da',
    '#c397d8',
    '#70c0b1',
    '#ffffff'
  },
  indexed = {
    [16] = '#de935f',
    [17] = '#a3685a',
    [18] = '#282a2e',
    [19] = '#373b41',
    [20] = '#b4b7b4',
    [21] = '#e0e0e0'
  },

--   tab_bar = {
--     -- The color of the strip that goes along the top of the window
--     -- (does not apply when fancy tab bar is in use)
--     background = '#303030',

--     -- The active tab is the one that has focus in the window
--     active_tab = {
--       -- The color of the background area for the tab
--       bg_color = '#373b41',
--       -- The color of the text for the tab
--       fg_color = '#c0c0c0',

--       -- Specify whether you want "Half", "Normal" or "Bold" intensity for the
--       -- label shown for this tab.
--       -- The default is "Normal"
--       intensity = 'Normal',

--       -- Specify whether you want "None", "Single" or "Double" underline for
--       -- label shown for this tab.
--       -- The default is "None"
--       underline = 'None',

--       -- Specify whether you want the text to be italic (true) or not (false)
--       -- for this tab.  The default is false.
--       italic = false,

--       -- Specify whether you want the text to be rendered with strikethrough (true)
--       -- or not for this tab.  The default is false.
--       strikethrough = false,
--     },

--     -- Inactive tabs are the tabs that do not have focus
--     inactive_tab = {
--       bg_color = '#282a2e',
--       fg_color = '#808080',

--       -- The same options that were listed under the `active_tab` section above
--       -- can also be used for `inactive_tab`.
--     },

--     -- You can configure some alternate styling when the mouse pointer
--     -- moves over inactive tabs
--     inactive_tab_hover = {
--       bg_color = '#373b41',
--       fg_color = '#909090',
--       italic = true,

--       -- The same options that were listed under the `active_tab` section above
--       -- can also be used for `inactive_tab_hover`.
--     },

--     -- The new tab button that let you create new tabs
--     new_tab = {
--       bg_color = '#373b41',
--       fg_color = '#ffffff',

--       -- The same options that were listed under the `active_tab` section above
--       -- can also be used for `new_tab`.
--     },

--     -- You can configure some alternate styling when the mouse pointer
--     -- moves over the new tab button
--     new_tab_hover = {
--       bg_color = '#b4b7b4',
--       fg_color = '#000000',
--       italic = true,

--       -- The same options that were listed under the `active_tab` section above
--       -- can also be used for `new_tab_hover`.
--     },
--   },
}

-- The filled in variant of the < symbol
local SOLID_LEFT_ARROW = ''

-- The filled in variant of the > symbol
local SOLID_RIGHT_ARROW = ' '

-- This function returns the suggested title for a tab.
-- It prefers the title that was set via `tab:set_title()`
-- or `wezterm cli set-tab-title`, but falls back to the
-- title of the active pane in that tab.
function tab_title(tab_info)
  local title = tab_info.tab_title
  -- local id = tab_info.tab_id
  -- if the tab title is explicitly set, take that
  if title and #title > 0 then
    return title
  end
  return tab_info.tab_index+1
  -- Otherwise, use the title from the active pane
  -- in that tab
  -- return tab_info.active_pane.title
end

wezterm.on(
  'format-tab-title',
  function(tab, tabs, panes, config, hover, max_width)
    local edge_background = '#303030'
    local background = '#282a2e'
    local foreground = '#808080'

    if tab.is_active then
      background = '#373b41'
      foreground = '#c0c0c0'
    elseif hover then
      background = '#282a2e'
      foreground = '#909090'
    end

    local edge_foreground = background

    local title = tab_title(tab)

    -- ensure that the titles fit in the available space,
    -- and that we have room for the edges.
    title = wezterm.truncate_right(title, max_width - 2)

    return {
      { Background = { Color = edge_background } },
      { Foreground = { Color = edge_foreground } },
      { Text = SOLID_LEFT_ARROW },
      { Background = { Color = background } },
      { Foreground = { Color = foreground } },
      { Text = title },
      { Background = { Color = edge_background } },
      { Foreground = { Color = edge_foreground } },
      { Text = SOLID_RIGHT_ARROW },
    }
  end
)

config.window_frame = {
  active_titlebar_bg = '#1d1f21',
  inactive_titlebar_bg = '#333333'
}

wezterm.on('gui-startup', function(cmd)
  local tab, pane, window = mux.spawn_window(cmd or {})
  window:gui_window():maximize()
end)

return config
