" ==============================================================================
" nesting detection
let $INSIDE_VIFM = 'true'

set vicmd='nvim'
" set vicmd='emacs -nw'

" This makes vifm perform file operations on its own instead of relying on
" standard utilities like `cp`.  While using `cp` and alike is a more universal
" solution, it's also much slower when processing large amounts of files and
" doesn't support progress measuring.
set syscalls

" Trash Directory
" The default is to move files that are deleted with dd or :d to
" the trash directory.  If you change this you will not be able to move
" files by deleting them and then using p to put the file in the new location.
" I recommend not changing this until you are familiar with vifm.
" This probably shouldn't be an option.
set trash

" This is how many directories to store in the directory history.
set history=1000

" Automatically resolve symbolic links on l or Enter.
set nofollowlinks

" With this option turned on you can run partially entered commands with
" unambiguous beginning using :! (e.g. :!Te instead of :!Terminal or :!Te<tab>).
" set fastrun

" Natural sort of (version) numbers within text.
set sortnumbers

" sort by name, ignore case
" set sort=iname
set sort=-groups,dir,iname sortgroups='^(lib|Downloads|youtube)$'

" hide .. directory everywhere
set dotdirs=

" Maximum number of changes that can be undone.
set undolevels=100

" Use Vim's format of help file (has highlighting and "hyperlinks").
" If you would rather use a plain text help file set novimhelp.
set novimhelp

" don't allow automatic running of executable files
" set norunexec

" Selected color scheme
colorscheme dark

" Format for displaying time in file list. For example:
" TIME_STAMP_FORMAT=%m/%d-%H:%M
" See man date or man strftime for details.
set timefmt=%d/%m/%y\ %H:%M

" Show list of matches on tab completion in command-line mode
set wildmenu

" Display completions in a form of popup with descriptions of the matches
set wildstyle=popup

" Display suggestions in normal, visual and view modes for keys, marks and
" registers (at most 5 files).  In other view, when available.
set suggestoptions=normal,visual,view,otherpane,keys,marks,registers

" Ignore case in search patterns unless it contains at least one uppercase
" letter
set ignorecase
set smartcase

" Don't highlight search results automatically
set nohlsearch

" Use increment searching (search while typing)
set incsearch

" Try to leave some space from cursor to upper/lower border in lists
set scrolloff=0

" show preview window
" view
only
" vsplit

"set millerview
"set milleroptions+=lsize:1,csize:3,rsize:4,rpreview:all
" set milleroptions=lsize:1,csize:2

" Customize view columns a bit (enable ellipsis for truncated file names)
set viewcolumns=-{name},{target}..,6{}

" Set custom status line look
set statusline=' Hint: %z %= %A %8u:%-8g %E %13d '

" update terminal title
"set title

set nowrap

set wordchars=1-8,14-31,a-z,A-Z

" Empty the ruler. By default, it shows the number of directories+files.
set rulerformat='%l/%S %[+%x%] '

" What should be saved automatically between vifm sessions.  Drop "savedirs"
" value if you don't want vifm to remember last visited directories for you.
set vifminfo=dhistory,chistory,state,tui,shistory,
    \phistory,fhistory,dirstack,registers,bookmarks,bmarks,marks

" Makes it possible to copy files between servers
set syncregs=general

" Filter-out build and temporary files
" filter! /^.*\.(lo|o|d|class|py[co])$|.*~$/
"filter! /^\_\_MACOSX\/$/
filter node_modules\/

set grepprg='rg -n -H -j2 -g '!package-lock.json' %i %a %s'
set findprg='fd -t d -t f -H -L -E ".git" %A %s %u'
nnoremap ,f :find<space>

" Add additional place to look for executables
" let $PATH = $HOME.'/bin/fuse:'.$PATH

"au DirEnter '~/repos/**/*' setl previewprg='git log --color -- %c 2>&1'

" Don't show delete confirmation
"set confirm-=delete

" tmux integration
" screen!

set fusehome='$HOME/Library/.mount/fuse/'

" ------------------------------------------------------------------------------
" ==============================================================================
" mappings

" :mark mark /full/directory/path [filename]
mark h ~/
mark l ~/Library
mark a ~/Library/Anime
mark c ~/Library/Cloud
mark j ~/Library/Downloads
mark m ~/Library/Movies
mark s ~/Library/Series
mark z ~/.local/share/zinit

" ------------------------------------------------------------------------------
" ==============================================================================
" commands

" :com[mand][!] command_name action
" The following macros can be used in a command
" %a is replaced with the user arguments.
" %c the current file under the cursor.
" %C the current file under the cursor in the other directory.
" %f the current selected file, or files.
" %F the current selected file, or files in the other directory.
" %b same as %f %F.
" %d the current directory name.
" %D the other window directory name.
" %m run the command in a menu window

" Block particular shortcut
" nnoremap <left> <nop>

cnoremap <esc>[1;5C <a-f>
cnoremap <esc>[1;5D <a-b>

command! df df -h %m 2> /dev/null
command! diff git diff %f %F
command! zip :Bg zip -r -j %f.zip %f
" tar folder with zstd
command! zstdf :Bg tar -I 'zstd -T0 --ultra -22' -cf "%f".tar.zst -C "%f" .
nnoremap <silent> ,z :zstdf<cr>
command! run !! ./%f
command! make !!make %a
command! mkcd :mkdir %a | cd %a
command! vgrep nvim "+grep %a"
command! reload :write | restart
nnoremap ,r :reload<cr>:echo "↻"<cr>

" command! FZFlocate :set noquickview | :execute 'goto' fnameescape(term('fd -t d -F . $HOME | fzf --reverse 2>/dev/tty')) | view
" nnoremap <c-g> :FZFlocate<cr>
command! FZFfind :set noquickview | :execute 'goto' fnameescape(term('fd . -t f -t d -H -c always | fzf 2>/dev/tty'))
nnoremap F :FZFfind<cr>

" yank current directory path into the clipboard
nnoremap yd :!echo -n %d | wl-copy %i <cr>:echo expand('%"d') ": dir yanked to clipboard "<cr>
" yank current file path into the clipboard
nnoremap yp :!echo -n %c:p | wl-copy %i <cr>:echo expand('%"c:p') ": file path yanked to clipboard "<cr>
" yank current filename without path into the clipboard
nnoremap ye :!echo -n %c | wl-copy %i <cr>:echo expand('%"c') ": file & ext yanked to clipboard "<cr>
" yank root of current file's name into the clipboard
nnoremap yn :!echo -n %c:r | wl-copy %i <cr>:echo expand('%"c:r') ": file name yanked to clipboard "<cr>
" yank contents of file
nnoremap yc :!wl-copy -n < %c %i <cr>:echo expand('%"c') ": contents yanked "<cr>

" Push selected files via adb
" command! push :Bg adb push %f %i -- /sdcard/Movies
" command! push :Bg shell /home/mustaqim/.zsh/functions/push
command! push :! zsh /home/mustaqim/.zsh/functions/push
" command! push :! nu /home/mustaqim/.config/nushell/scripts/push.nu
nnoremap aa :push<cr>

" Extract an archive
command! extract :Bg dtrx -n \"%c\"
nnoremap <silent> x :extract<cr>
":echo expand('%"c') ": extracting archive "<cr>

" Tag a file
" command! tag tmsu tag --tags=\"%a\" %f %i
command! tag tmsu tag %f %a %i
nnoremap <silent> ,tt :tag<space>
nnoremap <silent> ,tm :!tmsu mount .mnt %i <cr>
nnoremap <silent> ,tu :!tmsu unmount .mnt %i <cr>

" command! imgcat wezterm imgcat %f
" nnoremap av :imgcat<cr>

" Start shell in current directory
nnoremap s :shell!<cr>

" Display sorting dialog
nnoremap S :sort<cr>

" Toggle visibility of preview window
nnoremap w :view<cr>
vnoremap w :view<cr>gv

" Open file in existing instance
nnoremap o :!emacsclient -nc %f &<cr>
" Open file in new instance
nnoremap O :!emacs -bg "#1d1f21" -mm %f &<cr>

" Mappings for faster renaming
nnoremap I cw<c-a>
nnoremap cc cW<c-u>
nnoremap A cw
nnoremap ,a cw.BAK<cr>

" Open editor to edit vifmrc and apply settings after returning to vifm
nnoremap ,c :write | edit $MYVIFMRC | restart<cr>
" Open gvim to edit vifmrc
"nnoremap ,C :vicmd--remote-tab-silent $MYVIFMRC &<cr>

" Toggle wrap setting on ,w key
nnoremap ,w :set wrap!<cr>

nnoremap <f1> :tabnew<cr>
nnoremap <f2> :tabname<space>
nnoremap > gt
nnoremap < gT
nnoremap <f3> :!batcat %f --color always --paging always --pager=less <cr>
nnoremap <f4> :edit<cr>
nnoremap <f5> :copy<cr>
nnoremap <f6> :move<cr>
nnoremap <f7> :mkdir<space>
nnoremap mkd  :mkdir<space>

nmap q ZQ
nnoremap ; :

" Upload highlighted file to 0x0.st and then save url to clipboard
" nnoremap 0x0 :!curl -s -F'file=@%c' https://0x0.st > /dev/null | xclip -sel clip && notify-send "vifm" "File uploaded: $(xclip -o -selection clipboard)" &<cr>

"nnoremap gg 2gg
"nnoremap t tj

" Open file in the background using its default program
nnoremap gb :file &<cr>l

" Open application dialog
nnoremap b :file &<cr> %i

" brief information about files
nnoremap <silent> ,b :set viewcolumns=*{name}..,6{}.<cr>
" detailed information about files
nnoremap <silent> ,d :set viewcolumns=*{name}.,10{perms},12{uname},-7{gname},10{size}.,20{mtime}<cr>


" ------------------------------------------------------------------------------
" ==============================================================================
" file preview & file opening

" don't show preview on ../
" fileviewer ../ echo >/dev/null

" Directories
" filextype */
"        \ {View in thunar}
"        \ Thunar %f &,
" fileviewer */,.*/ lsd --icon never --color=always --group-dirs=first --tree --depth=1 -a
" fileviewer */,.*/ ls --color -A
" fileviewer */,.*/ tree -L 1 -C

" Pdf
filextype *.pdf zathura %c %i
" fileviewer *.pdf
"         \ vifmimg pdf %px %py %pw %ph %c
"         \ %pc
"         \ vifmimg clear

" Comic
filextype *.cbr,*.cbz zathura %c %i
" fileviewer *.cbr,*.cbz
"         \ vifmimg comic %px %py %pw %ph %c
"         \ %pc
"         \ vifmimg clear

" epub
filextype *.epub zathura %c %i &
" fileviewer *.epub
"         \ vifmimg epub %px %py %pw %ph %c
"         \ %pc
"         \ vifmimg clear
" mobi
"filextype *.mobi zathura %c %i
" fileviewer *.mobi
"         \ vifmimg mobi %px %py %pw %ph %c
"         \ %pc
"         \ vifmimg clear

" PostScript
filextype *.ps,*.eps,*.ps.gz
        \ {View in zathura}
        \ zathura %f,

" Djvu
filextype *.djvu
        \ {View in zathura}
        \ zathura %f,
        \ {View in apvlv}
        \ apvlv %f,

" Audio
" filextype *.wav,*.mp3,*.flac,*.m4a,*.wma,*.ape,*.ac3,*.og[agx],*.spx,*.opus
filetype <audio/*>
       \ {Play using mpv}
       \ mpv --player-operation-mode=pseudo-gui -- %f %i &,
       " \ {Play using ffplay}
       " \ ffplay -nodisp -autoexit %c &,
" fileviewer <audio/*>
"         \ vifmimg audio %px %py %pw %ph %c
"         \ %pc
"         \ vifmimg clear

" Video
" filextype *.avi,*.mp4,*.wmv,*.dat,*.3gp,*.ogv,*.mkv,*.mpg,*.mpeg,*.vob,
"         \*.fl[icv],*.m2v,*.mov,*.webm,*.ts,*.mts,*.m4v,*.r[am],*.qt,*.divx,
"         \*.as[fx]
"        \ {View using mpv}
"        \ mpv %f %i &,
"        \ {View using ffplay}
"        \ ffplay -fs -autoexit %f,
"        \ {View using Dragon}
"        \ dragon %f:p,
"fileviewer *.avi,*.mp4,*.wmv,*.dat,*.3gp,*.ogv,*.mkv,*.mpg,*.mpeg,*.vob,
""          \*.fl[icv],*.m2v,*.mov,*.webm,*.ts,*.mts,*.m4v,*.r[am],*.qt,*.divx,
""          \*.as[fx]
""         \ ffprobe -pretty %c 2>&1

filextype <video/*>
         \ {View using mpv}
         \ mpv --player-operation-mode=pseudo-gui -- %f %i &,
filextype *.m2ts
         \ {View using mpv}
         \ mpv --player-operation-mode=pseudo-gui -- %f %i &,
fileviewer <video/*>
       \ mediainfo %c
"          \ vifmimg video %px %py %pw %ph %c
"          \ %pc
"          \ vifmimg clear

" html
filextype *.html,*.htm
        \ {Open with Firefox}
        \ firefox %f &,
        " \ {Open with vimb}
        " \ vimb -c ~/.config/vimb/settings.conf %f %i &,
        " \ {Open with surf}
        " \ surf %f %i &,
        " \ {Open with uzbl}
        " \ uzbl-browser %f %i &,
"filetype *.html,*.htm links, lynx
"filextype *.html,*.htm firefox %f 2>/dev/null
fileviewer *.html w3m -dump %c

" Fonts
" fileviewer *.otf,*.ttf,*.woff file %c
"        \ vifmimg font %px %py %pw %ph %c
"        \ %pc
"        \ vifmimg clear

" Object
filetype *.o nm %f | less

" Man page
filetype *.[1-8] man ./%c
fileviewer *.[1-8] man ./%c | col -b

" Images
filetype <image/svg+xml>
       \ {View in qview}
       \ qview %f %i &,
" filetype *.bmp,*.gif,*.jpeg,*.jpg,*.jxl,*.png,*.svg,*.tif,*.webp,*.xpm
filetype <image/*>
       " \ {View in qview}
       " \ qview %f %i &,
       " \ {View in swayimg}
       " \ swayimg %f %i
       " \ {View in pqiv}
       " \ pqiv * &,
       " \ {View in gThumb}
       " \ gthumb %f %i &,
       " \ {View in imv}
       " \ imv -b 000000 * -n %f %i &,
       \ {View in feh}
       \ feh -. -F -B 'black' --start-at %c %i &,
       \ {View in Gnome Image Viewer}
       \ eog %f &,
       " \ {View in sxiv}
       " \ sxiv %f &,
" fileviewer <image/*>
           " \ wezterm imgcat %f
           " \ kitty +icat --silent --transfer-mode=stream --place=%pwx%ph@%pxx%py %c %N
           " \ %pc
           " \ kitty icat --clear --silent %pd

" OpenRaster
filextype *.ora
        \ {Edit in MyPaint}
        \ mypaint %f,

" Mindmap
filextype *.vym
        \ {Open with VYM}
        \ vym %f &,

" MD5
filetype *.md5
       \ {Check MD5 hash sum}
       \ md5sum -c %f %S,

" SHA1
filetype *.sha1
       \ {Check SHA1 hash sum}
       \ sha1sum -c %f %S,

" SHA256
filetype *.sha256
       \ {Check SHA256 hash sum}
       \ sha256sum -c %f %S,

" SHA512
filetype *.sha512
       \ {Check SHA512 hash sum}
       \ sha512sum -c %f %S,

" GPG signature
filetype *.asc
       \ {Check signature}
       \ !!gpg --verify %c,

" Torrent
" filetype *.torrent ktorrent %f &
fileviewer *.torrent imdl -c always -t torrent show -i %c

filetype *.jar java -jar %f %i &

" FuseZipMount
filetype *.jar,*.zip,*.zst,*.war,*.ear,*.oxt,*.apkg,*.deb,*.rpm,*.xbps,*.crate,*.xz
       \ {Mount with fuse-archive}
       \ FUSE_MOUNT|fuse-archive %SOURCE_FILE %DESTINATION_DIR,
       \ {View contents}
       \ zip -sf %c | less,
       \ {Extract here}
       \ unzip -qq %c,
fileviewer *.zip,*.jar,*.war,*.ear,*.oxt zipinfo -1 %c

" ArchiveMount
filetype *.gz,*.tar,*.tar.bz2,*.tbz2,*.tgz,*.tar.gz,*.tar.xz,*.tar.zst,*.txz
       \ {Mount with fuse-archive}
       \ FUSE_MOUNT|fuse-archive %SOURCE_FILE %DESTINATION_DIR,
fileviewer *.tgz,*.tar.gz tar -tzf %c
fileviewer *.tar.bz2,*.tbz2 tar -tjf %c
fileviewer *.tar.txz,*.txz xz --list %c
fileviewer *.tar tar -tf %c

" Rar2FsMount and rar archives
filetype *.rar
       \ {Mount with fuse-archive}
       \ FUSE_MOUNT|fuse-archive %SOURCE_FILE %DESTINATION_DIR,
fileviewer *.rar unrar v %c

" IsoMount
filetype *.iso
       \ {Mount with fuse-archive}
       \ FUSE_MOUNT|fuse-archive %SOURCE_FILE %DESTINATION_DIR,
fileviewer *.iso iso-info %c | tail -n5

" SshMount
filetype *.ssh
       \ {Mount with sshfs}
       \ FUSE_MOUNT2|sshfs %PARAM %DESTINATION_DIR %FOREGROUND,

" FtpMount
filetype *.ftp
       \ {Mount with curlftpfs}
       \ FUSE_MOUNT2|curlftpfs -o ftp_port=-,,disable_eprt %PARAM %DESTINATION_DIR %FOREGROUND,

" Fuse7z and 7z archives
filetype *.7z
       \ {Mount with fuse-archive}
       \ FUSE_MOUNT|fuse-archive %SOURCE_FILE %DESTINATION_DIR,
fileviewer *.7z 7z l %c

" Office files
filextype *.odt,*.doc,*.docx,*.xls,*.xlsx,*.odp,*.pptx libreoffice %f &
fileviewer *.doc catdoc %c
fileviewer *.docx docx2txt.pl %f -

" TuDu files
filetype *.tudu tudu -f %c

" Qt projects
filextype *.pro qtcreator %f &

" fileviewer *.json python -m json.tool -- %c | highlight -q -O xterm256 --replace-tabs=2 --style=base16/tomorrow-night --syntax json
fileviewer <application/json> batcat -p --color always %c
" fileviewer *.org highlight -q -O xterm256 --replace-tabs=2 --style=base16/tomorrow-night --syntax md
fileviewer *.[s]o hexyl -c 500 %c

" Text
filetype <text/*> emacs
fileviewer <text/*> batcat -p --color=always %c
fileviewer <application/javascript> batcat -p --color=always %c
" fileviewer <text/*> tree-sitter highlight %c
" fileviewer <text/*> highlight -q -O truecolor --replace-tabs=2 --style=base16/tomorrow-night %c

filetype * xdg-open %c &
" filetype * mimeo %c %i &
" fileviewer * file -b %c

set classify='  :dir:,  :exe:,  :reg:,  :link:,  ::*.exe::,  ::*.apk::'
" code
set classify+='  ::../::,  ::*.nu,,*.sh,,*.zsh::,󰙲  ::*.[hc]pp::,  ::*.c::,  ::*.h::,  ::*.dart::,  ::/^copying|licen[cs]e|NOTICE$/::,  ::*.py,,*.ipynb::,  ::*.rkt::,  ::/hdl$/::'
set classify+='  ::.git/,,*.git/,,.gitignore,,.gitconfig,,.gitattributes,,.github/::,  ::*.gradle::,  ::*.htm,,*.html,,**.[sx]html::,  ::*.xml::,  ::*.css,,*.scss::'
set classify+='  ::*.gd,,*.godot,,*.gdns,,*.tscn,,*.tres::,  ::*.java::, ::*.go::,  ::*.lua::,  ::*.vue::,  ::*.jar::,  ::*.rb,,Gemfile::,  ::*.kt::'
set classify+='  ::*.diff,,*.patch::,  ::*.tex::,  ::*.el,,*.elc,,*.eln::,  ::*.cls,,*.sty::,  ::Dockerfile::,  ::*.rs::,  ::*.test.js::,  ::*.js::,  ::*.jsx::,  ::*.ts::,  ::*.tsx::'
set classify+='  ::*.conf,,*.ini,,*.cfg,,*.config,,config::,  ::/^rc|rc$/::,  ::*.nix::,  ::*.properties::,  ::*.log::,  ::/json|yml$/::,  ::Cargo.toml::,  ::*.toml::'
set classify+='  ::*.db,,*.sql,,*.sqlite::'
set classify+='  ::*.pem::,  ::*.lock::'
" archives
set classify+='  ::*.7z*,,*.ace,,*.arj,,*.bz2,,*.cpio,,*.crate,,*.deb,,*.dz,,*.gz,,*.lzh,,*.lzma,,*.rar,,*.rpm,,*.rz,,*.tar,,*.xbps,,*.zst::'
set classify+='  ::*.snap,,*.taz,,*.tb2,,*.tbz,,*.tbz2,,*.tgz,,*.tlz,,*.trz,,*.txz,,*.tz,,*.tz2,,*.xz,,*.z,,*.zip,,*.zoo::'
set classify+='  ::*.iso,,*.img::'
" images
set classify+='  ::*.ai,,*.avif,,*.bmp,,*.gif,,*.jpeg,,*.jpg,,*.jxl,,*.ico,,*.png,,*.ppm,,*.svg,,*.svgz,,*.tga,,*.tif,,*.tiff,,*.webp,,*.xbm,,*.xcf,,*.xpm,,*.xspf,,*.xwd::'
" audio
set classify+='  ::*.aac,,*.anx,,*.asf,,*.au,,*.axa,,*.eac3,,*.flac,,*.m2a,,*.m4a,,*.mid,,*.midi,,*.mp3,,*.mpc,,*.oga,,*.ogg,,*.ogx,,*.opus,,*.ra,,*.ram,,*.rm,,*.spx,,*.wav,,*.wma,,*.ac3::'
set classify+='  ::*.m3u::'
" media
set classify+='  ::*.avi,,*.axv,,*.divx,,*.m2v,,*.m2ts,,*.m4p,,*.m4v,,.mka,,*.mkv,,*.mov,,*.mp4,,*.flv,,*.mp4v,,*.mpeg,,*.mpg,,*.nuv,,*.ogv,,*.pbm,,*.pgm,,*.qt,,*.vid,,*.vob,,*.wmv,,*.xvid,,*.webm::'
" documents
set classify+='  ::*.org::,  ::*.md::,  ::*.doc,,*.docx::,  ::*.xls,,*.xls[mx]::,  ::*.pptx,,*.ppt::,  ::*.msg,,*.text,,*.txt,,README::'
set classify+='  ::*.epub,,*.fb2,,*.djvu,,*.mobi::'
set classify+='  ::*.csv::,  ::*.srt,,*.vtt::,  ::*.pdf::,  ::*.cbr,,*.cbz::'
" misc
set classify+='  ::*.otf,,*.ttf::,  ::*.asc::,  ::.priv-env::'
"}}}

