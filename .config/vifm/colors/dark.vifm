" -*- mode:vimrc; -*-

highlight clear

highlight AuxWin cterm=none ctermfg=248 ctermbg=default
highlight Border cterm=none ctermfg=black ctermbg=234
highlight BrokenLink cterm=reverse ctermfg=red ctermbg=default
highlight CmdLine cterm=none ctermfg=white ctermbg=default
highlight CurrLine cterm=none ctermfg=default ctermbg=235
highlight Device cterm=bold ctermfg=red ctermbg=default
highlight Directory cterm=none ctermfg=102 ctermbg=234
highlight ErrorMsg cterm=none ctermfg=red ctermbg=black
highlight Executable cterm=none ctermfg=208 ctermbg=default
highlight Fifo cterm=bold ctermfg=cyan ctermbg=default
highlight JobLine cterm=bold ctermfg=blue ctermbg=black
highlight Link cterm=none ctermfg=default ctermbg=234
highlight OtherLine cterm=none ctermfg=none ctermbg=235
highlight OtherWin cterm=none ctermfg=248 ctermbg=default
highlight Selected cterm=none ctermfg=255 ctermbg=236
highlight Socket cterm=bold ctermfg=125 ctermbg=default
highlight StatusLine cterm=none ctermfg=8 ctermbg=235
highlight SuggestBox cterm=bold ctermfg=default ctermbg=default
highlight TabLine cterm=none ctermfg=white ctermbg=234
highlight TabLineSel cterm=bold ctermfg=white ctermbg=236
highlight TopLine cterm=bold ctermfg=8 ctermbg=234
highlight TopLineSel cterm=bold ctermfg=20 ctermbg=default
highlight WildMenu cterm=underline,reverse ctermfg=237 ctermbg=white
highlight Win cterm=none ctermfg=248 ctermbg=234


" file name specific highlight
highlight /^.*(\.bash_login|\.bash_logout|\.bash_profile|\.entitlements|\.epf|\.hidden-color-scheme|\.hidden-tmTheme|\.ini|\.last-run|\.merged-ca-bundle|\.pbxproj|\.pcf|\.plist|\.profile|\.psf|\.rstheme|\.strings|\.sublime-build|\.sublime-commands|\.sublime-keymap|\.sublime-project|\.sublime-settings|\.sublime-snippet|\.sublime-workspace|\.tmTheme|\.user-ca-bundle|\.viminfo|\.xcconfig|\.xcsettings|\.xcuserstate|\.xcworkspacedata|\.zlogin|\.zlogout|\.zprofile|\.zshenv|authorized_keys|cfg|conf|config|known_hosts|rc)$/I cterm=none ctermfg=default ctermbg=default
highlight /^.*\.(cjs|js|jsx|mjs|ts|tsx)$/I cterm=none ctermfg=074 ctermbg=default
highlight /^.*\.(coffee|java|jsm|jsp)$/I cterm=none ctermfg=079 ctermbg=default
highlight /^.*\.(css|less|sass|scss)$/I cterm=none ctermfg=105 ctermbg=default
highlight /^.*\.(Rproj)$/I cterm=none ctermfg=11 ctermbg=default
highlight /^.*\.(H|M|S|agdai|h|h++|hi|hpp|hxx|ii|m|s|tcc)$/I cterm=none ctermfg=110 ctermbg=default
highlight /^.*\.(doc|docx|odb|odt|pages|rtf)$/I cterm=none ctermfg=111 ctermbg=default
highlight /^.*\.(docm)$/I cterm=underline ctermfg=111 ctermbg=default
highlight /^.*\.(allow|numbers|ods|xls|xlsx)$/I cterm=none ctermfg=112 ctermbg=default
highlight /^.*\.(xlsxm)$/I cterm=underline ctermfg=112 ctermbg=default
highlight /^.*\.(IFO|MOV|avi|divx|m2v|m4v|mkv|mov|mp4|mpeg|mpg|ogm|qt|rmvb|sample|t|wmv)$/I cterm=none ctermfg=114 ctermbg=default
highlight /^.*\.(3g2|3gp|asf|f4v|flv|gp3|gp4|ogv|webm)$/I cterm=none ctermfg=115 ctermbg=default
highlight /^.*\.(VOB|vob)$/I cterm=none ctermfg=115 ctermbg=default
highlight /^.*\.(application|cue|description|directory|m3u|m3u8|md5|properties|sfv|theme|torrent|urlview|webloc)$/I cterm=none ctermfg=116 ctermbg=default
highlight /^.*\.(ass|srt|ssa|sub|sup|vtt)$/I cterm=none ctermfg=117 ctermbg=default
highlight /^.*\.(82p|83p|8eu|8xe|8xp)$/I cterm=none ctermfg=121 ctermbg=default
highlight /^.*\.(bin|fvd|img|iso|nrg|qcow|sparseimage|toast|vcd|vdi|vfd|vhd|vhdx|vmdk)$/I cterm=none ctermfg=red ctermbg=default
highlight /^.*\.(htm|html|jhtm|mht)$/I cterm=bold ctermfg=125 ctermbg=default
highlight /^.*\.(astro|ejs|mustache|pug|svelte|vue)$/I cterm=none ctermfg=135 ctermbg=default
highlight /^.*\.(comp|frag|vert)$/I cterm=none ctermfg=136 ctermbg=default
highlight /^.*\.(aiff|alac|ape|cda|flac|mid|midi|pcm|wav|wv|wvc)$/I cterm=none ctermfg=136 ctermbg=default
highlight /^.*\.(3ga|S3M|aac|amr|au|caf|dat|dts|fcm|m4a|mod|mp3|mp4a|oga|ogg|opus|s3m|sid|wma)$/I cterm=none ctermfg=137 ctermbg=default
highlight /^.*\.(PDF|cbr|cbz|chm|djvu|epub|mobi|pdf)$/I cterm=none ctermfg=141 ctermbg=default
highlight /^.*\.(libsonnet)$/I cterm=none ctermfg=142 ctermbg=default
highlight /^.*(Containerfile|Dockerfile|Makefile|\.nix|\.rake)$/I cterm=none ctermfg=155 ctermbg=default
highlight /^.*\.(BAT|awk|bash|bat|fish|kak|sed|sh|vim|zsh)$/I cterm=none ctermfg=16 ctermbg=default
highlight /^.*\.(PL)$/I cterm=none ctermfg=160 ctermbg=default
highlight /^.*\.(err|error|stderr)$/I cterm=none ctermfg=160 ctermbg=default
highlight /^.*\.(iml|key|odp|pps|ppt|ppts|pptx)$/I cterm=none ctermfg=166 ctermbg=default
highlight /^.*\.(pptsm|pptxm)$/I cterm=underline ctermfg=166 ctermbg=default
highlight /^.*\.(tf|tfstate|tfvars)$/I cterm=none ctermfg=168 ctermbg=default
highlight /^.*\.(RData|bib|dhall|dtd|fxml|geojson|hjson|json|json5|jsonc|jsonl|jsonnet|msg|ndjson|pgn|rdata|rnc|rng|rss|sgml|toml|xml|xsd|yaml|yml)$/I cterm=none ctermfg=178 ctermbg=default
highlight /^.*\.(adoc|asciidoc|etx|info|markdown|md|mdx|mkd|nfo|norg|org|pod|rst|tex|textile)$/I cterm=none ctermfg=11 ctermbg=default
highlight /^.*\.(log)$/I cterm=none ctermfg=190 ctermbg=default
highlight /^.*(\.asc|\.bfe|\.enc|\.gpg|\.p12|\.p7s|\.pem|\.pgp|\.sig|\.signature|id_dsa|id_ecdsa|id_ed25519|id_rsa)$/I cterm=none ctermfg=192 ctermbg=default
highlight /^.*\.(git|github)$/I cterm=none ctermfg=197 ctermbg=default
highlight /^.*\.(pl|xib)$/I cterm=none ctermfg=208 ctermbg=default
highlight /^.*\.(32x|A64|a00|a52|a64|a78|adf|atr|cdi|fm2|gb|gba|gbc|gel|gg|ggl|ipk|j64|nds|nes|rom|sav|sms|st)$/I cterm=none ctermfg=213 ctermbg=default
highlight /^.*\.(apk|bsp|cab|crx|crate|deb|dmg|ear|ipa|jad|jar|pak|pk3|rpm|vdf|vpk|war|xbps|xpi)$/I cterm=none ctermfg=215 ctermbg=default
highlight /^.*\.(dwg|ply|stl|wrl)$/I cterm=none ctermfg=216 ctermbg=default
highlight /^.*\.(spv)$/I cterm=none ctermfg=217 ctermbg=default
highlight /^.*\.(scpt|swift)$/I cterm=none ctermfg=219 ctermbg=default
highlight /^.*(AUTHORS|CHANGELOG|CHANGELOG.md|CHANGES|CODEOWNERS|CONTRIBUTING|CONTRIBUTING.md|CONTRIBUTORS|COPYING|COPYRIGHT|HISTORY|INSTALL|LICENSE|LICENSE.md|NOTICE|PATENTS|README|README.md|README.rst|VERSION)$/I cterm=none ctermfg=11 ctermbg=default
highlight /^.*\.(msql|mysql|pgsql|prisma|prql|sql)$/I cterm=none ctermfg=222 ctermbg=default
highlight /^.*(CodeResources|PkgInfo|\.CFUserTextEncoding|\.DS_Store|\.localized|\.part|\.r[0-9]{0,2}|\.z[0-9]{0,2}|\.zx[0-9]{0,2}|!qB)$/I cterm=none ctermfg=239 ctermbg=default
highlight /^.*(\.containerignore\.dockerignore|\.gitattributes|\.gitignore|\.gitmodules|\.pyc|pm_to_blib)$/I cterm=none ctermfg=240 ctermbg=default
highlight /^.*(\.BUP|\.BAK|\.aria2|\.bak|\.disable|\.dll|\.dump|\.dylib|\.elc|\.eln|\.mdump|\.o|\.orig|\.rlib|\.stackdump|\.un~|\.zcompdump|\.zwc|core)$/I cterm=none ctermfg=241 ctermbg=default
highlight /^.*\.(am|hin|in|m4|old|out|scan)$/I cterm=none ctermfg=242 ctermbg=default
highlight /^.*(MANIFEST)$/I cterm=none ctermfg=243 ctermbg=default
highlight /^.*\.(SKIP|sassc|swo|swp|tmp)$/I cterm=none ctermfg=244 ctermbg=default
highlight /^.*(\.pid|\.state|lock|lockfile)$/I cterm=none ctermfg=248 ctermbg=default
highlight /^.*\.(txt)$/I cterm=none ctermfg=253 ctermbg=default
highlight /^.*\.(cap|dmp|pcap)$/I cterm=none ctermfg=29 ctermbg=default
highlight /^.*\.(pacnew)$/I cterm=none ctermfg=33 ctermbg=default
highlight /^.*\.(lnk)$/I cterm=none ctermfg=39 ctermbg=default
highlight /^.*\.(7z|WARC|a|arj|br|bz2|cpio|gz|lrz|lz|lzma|lzo|rar|s7z|sz|tar|tbz|tgz|warc|xz|z|zip|zipx|zoo|zpaq|zst|zstd|zz)$/I cterm=none ctermfg=40 ctermbg=default
highlight /^.*\.(ahk|clj|cljc|cljs|cljw|gemspec|ipynb|py|rb|sc|scala|xsh)$/I cterm=none ctermfg=41 ctermbg=default
highlight /^.*(@.service|\.automount|\.device|\.mount|\.path|\.service|\.snapshot|\.socket|\.swap|\.target|\.timer)$/I cterm=none ctermfg=45 ctermbg=default
highlight /^.*\.(R|r)$/I cterm=none ctermfg=49 ctermbg=default
highlight /^.*\.(dart)$/I cterm=none ctermfg=51 ctermbg=default
highlight /^.*\.(car|nib)$/I cterm=none ctermfg=57 ctermbg=default
highlight /^.*\.(accdb|accde|accdr|accdt|db|fmp12|fp7|localstorage|mdb|mde|nc|sqlite|typelib)$/I cterm=none ctermfg=60 ctermbg=default
highlight /^.*\.(tcl)$/I cterm=bold ctermfg=64 ctermbg=default
highlight /^.*\.(PFA|afm|fnt|fon|otf|pfa|pfb|pfm|ttf|woff|woff2)$/I cterm=none ctermfg=66 ctermbg=default
highlight /^.*\.(1p|3p|cnc|def|ex|example|feature|gbr|ger|ics|map|mf|mfasl|mi|mm|mtx|pc|pcb|pi|plt|pm|pot|rdf|ru|sch|scm|sis|spl|sty|sug|tdy|tfm|tfnt|tg|vcard|vcf|xcf|xln)$/I cterm=none ctermfg=7 ctermbg=default
highlight /^.*\.(xltx)$/I cterm=none ctermfg=73 ctermbg=default
highlight /^.*\.(xltm)$/I cterm=underline ctermfg=73 ctermbg=default
highlight /^.*\.(xla)$/I cterm=none ctermfg=76 ctermbg=default
highlight /^.*\.(csv|tsv)$/I cterm=none ctermfg=78 ctermbg=default
highlight /^.*\.(C|F|F03|F08|F90|F95|agda|asm|c|c++|cc|cl|cp|cpp|cr|cs|ctp|cxx|el|f|f03|f08|f90|f95|for|ftn|go|gs|hs|lagda|lagda.md|lagda.rst|lagda.tex|lhs|lisp|lua|ml|moon|nim|nimble|php|rkt|rs|sx|twig|v|vala|vapi|vb|vba|vbs|zig)$/I cterm=none ctermfg=39 ctermbg=default
highlight /^.*\.(deny|storyboard)$/I cterm=none ctermfg=9 ctermbg=default
highlight /^.*\.(eml|http)$/I cterm=none ctermfg=90 ctermbg=default
highlight /^.*\.(JPG|TIFF|avif|bmp|cdr|dicom|flif|gif|icns|ico|jpeg|jpg|jxl|nth|png|psd|pxd|pxm|tif|tiff|webp|wgsl|xpm)$/I cterm=none ctermfg=97 ctermbg=default
highlight /^.*\.(ai|drw|eps|epsf|ps|svg)$/I cterm=none ctermfg=99 ctermbg=default
highlight /^.*\.(diff)$/I cterm=none ctermfg=197 ctermbg=default
highlight /^.*\.(patch)$/I cterm=none ctermfg=197 ctermbg=default
highlight /^.*(LS_COLORS)$/I cterm=none ctermfg=197 ctermbg=default


if $USER == 'root'
    highlight Border ctermbg=red
endif

highlight User1 ctermfg=blue
