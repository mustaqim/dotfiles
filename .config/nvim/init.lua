require('plugins')

-- require'lspconfig'.denols.setup{}
-- require'lspconfig'.rust_analyzer.setup{}
-- require'lspconfig'.sumneko_lua.setup{}
 
-- require'nvim-treesitter.configs'.setup {
--   -- A list of parser names, or "all"
--   -- ensure_installed = { "lua", "rust", "javascript" },
 
--   -- Install parsers synchronously (only applied to `ensure_installed`)
--   sync_install = false,
 
--   -- Automatically install missing parsers when entering buffer
--   auto_install = true,
-- 
--   -- List of parsers to ignore installing (for "all")
--   -- ignore_install = { "javascript" },
 
--   highlight = {
--     -- `false` will disable the whole extension
--     enable = true,
-- 
--     -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
--     -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
--     -- the name of the parser)
--     -- list of language that will be disabled
--     disable = { "c", "rust" },
-- 
--     -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
--     -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
--     -- Using this option may slow down your editor, and you may see some duplicate highlights.
--     -- Instead of true it can also be a list of languages
--     additional_vim_regex_highlighting = false,
--   }
-- }
 
-- local cmp = require'cmp'
-- cmp.setup({
-- 	mapping = cmp.mapping.preset.insert({
--         ['<C-b>'] = cmp.mapping.scroll_docs(-4),
--         ['<C-f>'] = cmp.mapping.scroll_docs(4),
--         ['<C-Space>'] = cmp.mapping.complete(),
--         ['<C-e>'] = cmp.mapping.abort(),
--         ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
--     }),
--     sources = cmp.config.sources({
--         { name = 'nvim_lsp' },
--         -- { name = 'vsnip' }, -- For vsnip users.
--         -- { name = 'luasnip' }, -- For luasnip users.
--         -- { name = 'ultisnips' }, -- For ultisnips users.
--         -- { name = 'snippy' }, -- For snippy users.
--                                  },
--       {
--         { name = 'buffer' },
--     })
-- })
-- -- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
-- cmp.setup.cmdline('/', {
--                     mapping = cmp.mapping.preset.cmdline(),
--                     sources = {
--                       { name = 'buffer' }
--                     }
-- })
-- 
-- -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
-- cmp.setup.cmdline(':', {
--                     mapping = cmp.mapping.preset.cmdline(),
--                     sources = cmp.config.sources({
--                         { name = 'path' }
--                                                  }, {
--                         { name = 'cmdline' }
--                     })
-- })
-- 
-- -- Setup lspconfig.
-- local capabilities = require("cmp_nvim_lsp").default_capabilities()
-- -- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
-- -- require('lspconfig')['sumneko_lua'].setup {
-- --   capabilities = capabilities }
 
require('lualine').setup {
  options = {
    icons_enabled = false,
    section_separators = { left = '', right = '' },
    component_separators = { left = '', right = '' }
  },
  sections = {
    lualine_x = {'encoding', 'filetype'}
  }
}

require('base16-colorscheme')
vim.cmd('colorscheme base16-tomorrow-night')
