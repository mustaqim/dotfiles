return require('packer').startup(function(use)
    use 'wbthomason/packer.nvim' -- Package manager
--     use 'neovim/nvim-lspconfig' -- Configurations for Nvim LSP
--     use 'hrsh7th/cmp-nvim-lsp'
--     use 'hrsh7th/cmp-buffer'
--     use 'hrsh7th/cmp-path'
--     use 'hrsh7th/cmp-cmdline'
--     use 'hrsh7th/nvim-cmp'
--     use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
    use { 'RRethy/nvim-base16' }
    use {
      'nvim-lualine/lualine.nvim',
      requires = { 'kyazdani42/nvim-web-devicons', opt = true }
    }
end)

