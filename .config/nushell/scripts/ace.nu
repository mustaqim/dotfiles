export def main [url: string] {
  match $url {
    $it if $it =~ '^acestream.*' => { mpv --no-terminal --no-ytdl --no-resume-playback --no-save-position-on-quit $url }
    _ => { mpv --no-ytdl --no-terminal --no-resume-playback --no-save-position-on-quit acestream://($url) }
  }
}
