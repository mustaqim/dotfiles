def actions [] {
    [
      {value: "show", description: "Show the mount"},
      {value: "hide", description: "Hide the mount"}
    ]
}
const dirs = { cipher:'/home/mustaqim/Library/.cipher'
               mount: '/home/mustaqim/Library/.plain' }

export def main [action: string@actions] {
  match $action {
    "show" => { if (ls $dirs.mount | is-empty) {
      gocryptfs -q $dirs.cipher $dirs.mount -o -allow_other; vifm $dirs.mount
    } else vifm $dirs.mount },
    "hide" => { fusermount -u $dirs.mount },
  }
}
