export def --wrapped main [...arg] {
  let string = (wl-paste -n)
  aria2c --continue -x16 -s16 -k1M --summary-interval=0 --check-certificate=false ...$arg -- $string
}
