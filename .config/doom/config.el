;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "mustaqim malim"
      user-mail-address "git@mustaqim.ml")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
(setq doom-font (font-spec :family "Iosevka" :size 16 :weight 'normal)
      doom-variable-pitch-font (font-spec :family "Iosevka" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-tomorrow-night)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Library/Cloud/org/")

;; Hide titlebar
(add-to-list 'default-frame-alist '(undecorated . t))

;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.


(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; ============================================================================
;; automodes

(add-to-list 'auto-mode-alist '("\\template\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.profile\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.hdl\\'" . vhdl-mode))
(define-generic-mode vimrc-mode '("\"") '("set" "syntax") nil '("\\vifmrc\\'") nil
                     "Generic mode for Vim configuration files.")

(add-to-list 'auto-mode-alist '("\\.vue\\'" . web-mode))

(add-to-list 'Info-directory-list "/usr/local/share/info")


;; ===========================================================================
;; Vue.js

(defun my-web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-script-padding 0)
  (setq web-mode-code-indent-offset 2))
;; (setq web-mode-markup-indent-offset 0)
;; (setq web-mode-css-indent-offset 0)
(after! web-mode
  (add-hook 'web-mode-hook  'my-web-mode-hook)
  (add-to-list 'web-mode-indentation-params '("lineup-calls" . nil)))

;; (set-file-template! "\\.vue$" :trigger "__vueSFC" :mode 'web-mode)

;; (defun vuejs-custom ()
;;   (setq vue-html-tab-width 2)
;;   (flycheck-mode t)
;;   (rainbow-mode t)
;;   (global-set-key (kbd "C-c C-l") 'vue-mode-reparse)
;;   (global-set-key (kbd "C-c C-e") 'vue-mode-edit-indirect-at-point)
;;   (add-to-list 'write-file-functions 'delete-trailing-whitespace)
;;   (turn-on-diff-hl-mode)
;;   (setq mmm-submode-decoration-level 0)
;; Needed for proper indentation in vue-mode
;;   (setq mmm-js-mode-enter-hook (lambda () (setq syntax-ppss-table nil)))
;;   (setq mmm-typescript-mode-enter-hook (lambda () (setq syntax-ppss-table nil)))
;;   )
;; (after! vue-mode
;;   (add-hook 'vue-mode-hook 'vuejs-custom))
;; (add-hook 'vue-mode-hook (lambda () (setq syntax-ppss-table nil)))

;; (after! rjsx-mode
;;   (add-hook! 'rjsx-mode-hook 'prettier-mode))

;; (with-eval-after-load 'flycheck
;;   (flycheck-add-mode 'javascript-eslint 'vue-mode)
;;   (flycheck-add-mode 'javascript-eslint 'web-mode))
;; (add-to-list 'flycheck-checkers 'rustic-clippy)
;; (add-hook 'flycheck-mode-hook #'flycheck-inline-mode)


;; ===========================================================================
;; Flutter
(after! dart-mode
  (defun flutter-after-save-action () (flutter-run-or-hot-reload))
  (add-hook 'dart-mode-hook
            (lambda ()
              (add-hook 'after-save-hook #'flutter-after-save-action nil t))))

(after! dap-mode
  (setq lsp-dart-dap-flutter-hot-reload-on-save t))


;; ===========================================================================
;; flycheck

;; (defun my-fly-mode-hook () (flycheck-inline-mode 't))
;; (with-eval-after-load 'flycheck
;;   (add-hook! 'flycheck-mode-hook  'my-fly-mode-hook))


;; ============================================================================
;; key bindings
;; (map! :n "M-n" 'mc/mark-all-dwim)
;; (map! "C-S-<mouse-1>" 'mc/add-cursor-on-click)

(map! :n "g h" 'evil-beginning-of-line)
(map! :n "g l" 'evil-end-of-line)

;; (map! :n "C-e" nil)

;; (map! :n "C-<f5>" 'dap-java-debug)

;; (map! "<f8>" '+treemacs/toggle)
;; (map! "<f9>" 'centaur-tabs-mode)


;; ============================================================================
;; lsp
(setq lsp-ui-doc-show-with-cursor t)
;; (setq lsp-ui-sideline-show-code-actions t)
(setq lsp-ui-doc-delay 0.01)
;; (setq lsp-enable-symbol-highlighting nil)
;; (setq underline-minimum-offset 5)
;; (setq-hook! 'rjsx-mode-hook +format-with-lsp nil)
;; (add-to-list 'exec-path "~/.local/bin/language-servers/kotlin-language-server/bin")

(setq racket-program "~/.local/bin/racket/bin/racket")


(add-to-list 'auto-mode-alist '("\\.ino\\'" . cpp-mode))

(use-package! lsp-tailwindcss)
