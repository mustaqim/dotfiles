#!/usr/bin/env zsh

ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"
[ ! -d $ZINIT_HOME ] && mkdir -p "$(dirname $ZINIT_HOME)"
[ ! -d $ZINIT_HOME/.git ] && git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
source "${ZINIT_HOME}/zinit.zsh"

# autoload -Uz _zinit
# (( ${+_comps} )) && _comps[zinit]=_zinit

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    zdharma-continuum/zinit-annex-bin-gem-node \
    zdharma-continuum/zinit-annex-patch-dl
    # zdharma-continuum/zinit-annex-rust
    # zdharma-continuum/zinit-annex-as-monitor

### End of Zinit's installer chunk
# ==============================================================================

fpath=(
  # /usr/share/zsh/site-functions
  $ZDOTDIR/functions
  "${fpath[@]}"
)

# ==============================================================================
setopt no_global_rcs
HISTFILE=${ZDOTDIR}/zsh_history
HISTSIZE=100000
SAVEHIST=100000
HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE=1                # all search results returned will be unique
setopt incappendhistory                                 # add commmand to history as soon as it's entered
setopt extendedhistory                                  # save command timestamp
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_SAVE_NO_DUPS                                # don't write duplicate entries in the history file
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_SPACE                                # prefix commands you don't want stored with a space
HISTORY_IGNORE="(builtin *|ace *|exit|icat *|ls|r|open|pwd|q|run-help *|streamlink *|x *|s *|cd *)"

setopt NO_HUP                                           # don't kill jobs
setopt NO_CHECK_JOBS
setopt correct                                          # spelling correction for commands
setopt autocd
setopt interactivecomments
setopt extended_glob
unsetopt rm_star_silent                                 # ask for confirmation for rm * or rm path/*

typeset -gA FAST_HIGHLIGHT_STYLES
FAST_HIGHLIGHT_STYLES[alias]="fg=blue"
FAST_HIGHLIGHT_STYLES[path]="fg=blue"
FAST_HIGHLIGHT_STYLES[path-to-dir]="fg=blue,underline"
FAST_HIGHLIGHT_STYLES[suffix-alias]="fg=blue"
FAST_HIGHLIGHT_STYLES[builtin]="fg=blue"
FAST_HIGHLIGHT_STYLES[function]="fg=blue"
FAST_HIGHLIGHT_STYLES[precommand]="fg=red,bg=default,underline,bold"
FAST_HIGHLIGHT_STYLES[command]="fg=blue"
# FAST_HIGHLIGHT_STYLES[commandseparator]="fg=16"
FAST_HIGHLIGHT_STYLES[comment]="fg=008,bold,italic"
FAST_HIGHLIGHT_STYLES[single-quoted-argument]="fg=yellow"
FAST_HIGHLIGHT_STYLES[double-quoted-argument]="fg=yellow"
FAST_HIGHLIGHT_STYLES[dollar-quoted-argument]="fg=yellow"
FAST_HIGHLIGHT_STYLES[single-hyphen-option]="fg=yellow"
FAST_HIGHLIGHT_STYLES[double-hyphen-option]="fg=yellow"
FAST_HIGHLIGHT_STYLES[variable]="fg=016"
HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND='bg=012,fg=232,bold'
zle_highlight=('paste:reverse')
# ==============================================================================

# unalias run-help
# unalias zplg

# === FUNCTIONS ===

# Compile to decrease startup speed (only if $1 is older than 4 hours)
_zcompare() {
  # - '#q' is an explicit glob qualifier that makes globbing work within zsh's [[ ]] construct.
  # - 'N' makes the glob pattern evaluate to nothing when it doesn't match (rather than throw a globbing error)
  # - '.' matches "regular files"
  # - 'mh+4' matches files (or directories or whatever) that are older than 4 hours.
  # setopt extendedglob local_options
  # if [[ -n "${1}"(#qN.mh+4) && (! -s "${1}.zwc" || "$1" -nt "${1}.zwc" ) ]]; then
  if [[ -s ${1} && ( ! -s ${1}.zwc || ${1} -nt ${1}.zwc) ]]; then
    ZINIT[COMPINIT_OPTS]=-C; zicompinit; zicdreplay
    zcompile ${1}
  fi
}
_zcomp() {
  zshrc="${ZDOTDIR}/.zshrc"
  zcompdump="${ZDOTDIR}/.zcompdump"
  zshenv="${ZDOTDIR}/.zshenv"
  zprofile="${ZDOTDIR}/.zprofile"
  zplugins="${ZDOTDIR}/.zplugins"

  _zcompare "$zshrc"
  _zcompare "$zcompdump"
  _zcompare "$zshenv"
  _zcompare "$zprofile"
  _zcompare "$zplugins"
}
# _zcomp
# for file in ~/.zsh/functions/**/*(.); _zcompare "$file"

zshaddhistory() { whence ${${(z)1}[1]} >| /dev/null || return 1 }

# _fzf_compgen_path() { command fd -L -td -tf -tl -H -E \".git\" . \"\$1\" 2> /dev/null }
# _fzf_compgen_dir() { command fd -L -td -H -E \".git\" . \"\$1\" 2> /dev/null }

# Ctrl-w - delete a full WORD (including colon, dot, comma, quotes...)
# my-backward-kill-word () {
#   # Add colon, comma, single/double quotes to word chars
#   local WORDCHARS='*?_-.[]~=/&;!#$%^(){}<>:,"'"'"
#   zle -f kill # Append to the kill ring on subsequent kills.
#   zle backward-kill-word
# zle -N my-backward-kill-word
# bindkey '^w' my-backward-kill-word

# fzf-open-file-or-dir() {
#   local cmd="_fzf_compgen_path -calways $(pwd)"
#   local out=$(eval $cmd | fzf-tmux --exit-0)

#   if [ -f "$out" ]; then
#     /bin/emacs -nw "$out" </dev/tty
#   elif [ -d "$out" ]; then
#     cd "$out"
#     zle accept-line
#   fi
# }

# scroll-and-clear-screen() {
#     printf '\n%.0s' {1..$LINES}
#     zle clear-screen
# }
# zle -N scroll-and-clear-screen
# bindkey '^l' scroll-and-clear-screen

handle_url() {
  if [[ "$LBUFFER" == *"acestream://"* ]]; then
    echo "\nHandling Acstream URL: \e[4m$fg[blue]$LBUFFER\e[0m"
    ace $LBUFFER
  else
    zle .self-insert
  fi
}
zle -N handle_url
bindkey '^J' handle_url


# === BINDINGS ===
# zle     -N        fzf-open-file-or-dir
# bindkey '^O'      fzf-open-file-or-dir

bindkey '^[[1;5C'   vi-forward-word
bindkey '^[[1;5D'   vi-backward-word
bindkey '^F'        vi-forward-word
bindkey '^B'        emacs-backward-word
bindkey '^A'        beginning-of-line
# bindkey '^E'        end-of-line
# bindkey '^O'        push-line-or-edit
bindkey '^[[P'      delete-char
bindkey '^W'        backward-kill-word
bindkey '^[[Z'      reverse-menu-complete
bindkey "^[[109;6u" run-help # C-S-m

# zle -N            autosuggest-accept
# bindkey '^F'      autosuggest-accept

# autoload -U       edit-command-line
# zle -N            edit-command-line
# bindkey '^x^e'    edit-command-line

# Fish-like search completions functionality
zmodload zsh/complist
setopt menucomplete
zstyle ':completion:*'               menu select=2 search

bindkey -M menuselect '/'            history-incremental-search-forward
bindkey -M menuselect '?'            history-incremental-search-backward
bindkey -M menuselect '^B'           vi-backward-char
bindkey -M menuselect '^F'           vi-forward-char

zstyle ':completion:*'               matcher-list '' \
       'm:{a-z\-}={A-Z\_}' \
       'r:[^[:alpha:]]||[[:alpha:]]=** r:|=* m:{a-z\-}={A-Z\_}' \
       'r:|?=** m:{a-z\-}={A-Z\_}'

zstyle ':completion:*'               completer _expand _complete _match _correct _approximate
zstyle ':completion:*:match:*'       original only
zstyle ':completion:*:approximate:*' max-errors 1 numeric

zstyle '*:matches'                   group 'yes'
zstyle ':completion:*'               group-name ''
# zstyle ':completion:*:*:-command-:*' group-order builtins functions aliases commands
zstyle ':completion:*'               group-order files directories

zstyle ':completion:*'               list-dirs-first true
zstyle ':completion:*'               auto-description
zstyle ':completion:*'               file-patterns '%p:globbed-files' '*(-/):directories' '*:all-files'

zstyle ':completion:*:manuals'       separate-sections true
zstyle ':completion:*:manuals.(^1*)' insert-sections   true

zstyle ':completion:*:corrections'   format '%F{green}  %d (errors: %e)  %f'
zstyle ':completion:*:descriptions'  format '%F{blue}  %d  %f'
zstyle ':completion:*:messages'      format '%B%F{magenta}  %U%d%u  %f%b'
zstyle ':completion:*:warnings'      format '%B%F{red} %Uno matches found%u %f%b'
# zstyle ':completion:*:default'       list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*'               list-colors 'ma=48;5;19;38;5;255'

zstyle ':completion:*:parameters'    ignored-patterns '_*'

zstyle ':completion:*'               use-cache on
zstyle ':completion:*'               cache-path "$XDG_CACHE_HOME/zsh/zcompcache"

zstyle ':completion:*:functions'     ignored-patterns '([-])*' # ignore builtin functions that start with -

autoload -U select-word-style
select-word-style bash # Delete a word at a time

autoload -Uz bracketed-paste-url-magic
zle -N bracketed-paste bracketed-paste-url-magic

# Load autoload shell functions on demand
autoload -Uz ace decode dl fz fzf_log gitignore kd magit mkcd plain push rv switch_theme yadm_log_diff
autoload zcalc

# generic completions for programs which understand GNU long options(--help)
zicompdef _gnu_generic aomenc ar aria2c bandwhich curl cwebp cjxl darkhttpd direnv docker \
  dunst emacs feh ffmpeg ffprobe flask fsck.ext4 fzf gocryptfs hexyl highlight histdb inkscape ktlint light lighttpd \
  lsd mimeo megadl mkfs.vfat nzbget notify-send pamixer pip pip3 pipx psmem pw-cli rofi rustc \
  tlmgr tlp tlp-stat \
  vue zstd

# zi creinstall -Q $ZDOTDIR/completions

# for comp ( yadm vifm ) { zicompdef _$comp $comp; }

source $HOME/.zsh/plugins.zsh

