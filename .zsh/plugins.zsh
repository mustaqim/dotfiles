#!/usr/bin/env zsh

# ==============================================================================
# === PLUGINS ===
# === THEME ===
PROMPT=$'\n'"%F{blue}~%f"$'\n'
# zi ice depth'1' lucid atinit'source ~/.zsh/.p10k-lean-8colors.zsh' nocd atload"!_p9k_do_nothing _p9k_precmd"
# zi light romkatv/powerlevel10k
zi lucid light-mode from"gh-r" bpick"*linux-gnu.tar.gz" nocompile sbin"starship" \
  atclone"./starship init zsh > init.zsh; zcompile init.zsh; \
  ./starship completions zsh > _starship" atpull"%atclone" for \
  "starship/starship"

zi wait lucid light-mode for \
  atclone'(){local f;cd -q →*;for f (*~*.zwc){zcompile -Uz -- ${f}};}' \
  atpull'%atclone' compile'.*fast*~*.zwc' atinit"ZINIT[COMPINIT_OPTS]=-C; zicompinit; zicdreplay" \
    zdharma-continuum/fast-syntax-highlighting \
  atinit"
    bindkey '^[OA' history-substring-search-up
    bindkey '^P'   history-substring-search-up
    bindkey '^[OB' history-substring-search-down
    bindkey '^N'   history-substring-search-down" \
      zsh-users/zsh-history-substring-search \
  compile'**/*.zsh' atload'!_zsh_autosuggest_start' \
    zsh-users/zsh-autosuggestions \
  atclone'zinit creinstall -q ~/.zsh/completions' blockf \
    zsh-users/zsh-completions

# zi lucid for \
#   larkery/zsh-histdb

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)g*]} ]]' for \
  "OMZ::plugins/git/git.plugin.zsh" \
  "OMZ::plugins/git-extras/git-extras.plugin.zsh" \
  "wfxr/forgit"

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)g*]} ]]'\
  from'gh-r' bpick"*linux-gnu*" nocompile sbin'**/*delta' for \
  "dandavison/delta"

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)g*]} ]]' \
 atinit' \
 zstyle :omz:plugins:ssh-agent identities GitHub GitLab; \
 zstyle :omz:plugins:ssh-agent quiet yes' for \
 "OMZ::plugins/ssh-agent/ssh-agent.plugin.zsh"

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)c*]} ]]' pick"init.sh" for \
  "b4b4r07/enhancd"

# zi ice depth=1
# zi light "jeffreytse/zsh-vi-mode"

# if [[ $(type -p fzf) ]] then
zinit pack"binary+keys" wait'1' multisrc"_fzf_completion" for fzf
  # atload" \
  # _fzf_compgen_path() { command fd -L -td -tf -tl -H -E \".git\" . \"\$1\" 2> /dev/null }; \
  # _fzf_compgen_dir() { command fd -L -td -H -E \".git\" . \"\$1\" 2> /dev/null }" for \
# fi

zi lucid light-mode nocompile wait'[[ -n ${ZLAST_COMMANDS[(r)fp*]} ]]' sbin'fpp' \
 atclone"cp -vf debian/usr/share/man/man1/fpp.1 $ZINIT[MAN_DIR]/man1" for \
  "facebook/PathPicker"

zi lucid light-mode wait"1" compile"*.zsh" nocompletions for \
  "hlissner/zsh-autopair"

# zi ice wait'[[ -n ${ZLAST_COMMANDS[(r)e*]} ]]' lucid svn
# zi snippet "OMZ::plugins/emacs"

zi lucid light-mode wait"1" reset \
  atclone"sed -i 's/38;5;30/38;5;4/g; s/38;5;172/38;5;16/g; s/38;5;196/38;5;9/g' LS_COLORS; \
  dircolors -b LS_COLORS > c.zsh" atpull'%atclone' pick"c.zsh" nocompile'!' \
  atload'zstyle ":completion:*:default" list-colors "${(s.:.)LS_COLORS}"' for \
  "trapd00r/LS_COLORS"

# zi light "Aloxaf/fzf-tab"
# zstyle ':completion:*:descriptions' format '[%d]'
# zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls -1 --color=always $realpath'

# zi ice from"gh-r" as"program" mv"direnv* -> direnv" \
#  atclone"./direnv hook zsh > zhook.zsh" atpull"%atclone" compile"zhook.zsh" src"zhook.zsh"
# zi light direnv/direnv

# if [[ ! -d ~/.rbenv/plugins ]] then
#   echo "Creating \$(rbenv root)/plugins"
#   mkdir -p ~/.rbenv/plugins
#   if [[ ! -d ~/.rbenv/plugins/ruby-build ]] then
#      echo "Cloning \$(rbenv root)/plugins/ruby-build"
#      git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
#   fi
# fi
# zi ice svn lucid \
#  wait'[[ -n ${ZLAST_COMMANDS[(r)rben*]} ]]' \
#  atload"POWERLEVEL9K_RBENV_PROMPT_ALWAYS_SHOW=true" \
#  unload"![[ ! -e Gemfile || ! -e Rakefile ]]"
# zi snippet "PZT::modules/ruby/"
#
# zi ice wait"3a" lucid wait"[[ -f Gemfile || -f Rakefile ]]" unload"[[ ! -f Gemfile ]]"
# zi snippet "OMZ::plugins/rbenv/rbenv.plugin.zsh"

# zi ice lucid wait"[[ -f package-lock.json ]]" atload"[[ -f .nvmrc ]] && nvm exec" # wait'[[ -n ${ZLAST_COMMANDS[(r)nv]} ]]'
# zi light sei40kr/zsh-lazy-nvm

# zi ice lucid wait'[[ -n ${ZLAST_COMMANDS[(r)np*]} ]]'
# zi snippet "OMZ::plugins/npm/npm.plugin.zsh"

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)pn*]} ]]' id-as"pnpm-completions" \
  mv"pnpm-completions -> _pnpm" as"completion" nocompile blockf atinit"source _pnpm" for \
  "https://raw.githubusercontent.com/SebastienWae/pnpm-completions/main/pnpm.zsh"

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)den*]} ]]'\
  from'gh-r' bpick"*linux-gnu*" nocompile sbin'*->deno' for \
  denoland/deno

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)tap*]} ]]'\
  from'gh-r' bpick"*-full-*" nocompile sbin'*->taplo' for \
  tamasfe/taplo

# zi ice lucid wait \
#  as'command' pick'bin/pyenv' \
#  atload'eval "$(pyenv init - --no-rehash zsh)"; echo "TEST"' \
#  src'completions/pyenv.zsh' nocompile'!'
# zi light pyenv/pyenv
# export PYENV_ROOT="$HOME/.pyenv"

# zi ice lucid wait'[[ -n ${ZLAST_COMMANDS[(r)pyenv]} ]]' \
#  as'command' pick"bin/*" \
#  atload'eval "$(pyenv virtualenv-init - zsh)"'
# zi light pyenv/pyenv-virtualenv

# zi ice wait'[[ -n ${ZLAST_COMMANDS[(r)ch*]} ]]' lucid as"program" mv"*cht.sh -> cht" pick"cht" id-as"cht.sh"
# zi snippet "https://cht.sh/:cht.sh"
# zi ice wait"2" lucid id-as"tldr" as"program" pick"tldr"
# zi snippet "https://raw.githubusercontent.com/raylee/tldr/master/tldr"

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)her*]} ]]' extract"!-" id-as"heroku" \
  nocompile sbin'bin/heroku' mv"bin/node -> bin/nodeBAK" for \
  "https://cli-assets.heroku.com/heroku-linux-x64.tar.gz"
zi ice lucid wait'[[ -n ${ZLAST_COMMANDS[(r)her*]} ]]'
zi snippet "OMZ::plugins/heroku/heroku.plugin.zsh"

# zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)gcloud*]} ]]' multisrc"{path.zsh,completion.zsh}.inc" id-as"gcloud" for \
#  "/snap/google-cloud-cli/current/"

# zi ice as"program" pick"bin/sml"
# zi snippet "~/.local/bin/sml/bin/sml"

# zi ice lucid from"gh-r" as"program"
# zi light "pinterest/ktlint"

# zi ice lucid blockf
# zi light "ziglang/shell-completions"

# zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)rust*]} ]]' \
#   from'gh-r' bpick"*linux-gnu.gz" nocompile sbin'*->rust-analyzer' for \
#   rust-lang/rust-analyzer

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)tree*]} ]]' \
  from'gh-r' nocompile sbin'*->tree-sitter' for \
  tree-sitter/tree-sitter

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)ju*]} ]]' \
  from'gh-r' nocompile mv"completions/just.zsh -> _just" sbin'just' for \
  casey/just

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)jl*]} ]]' \
  from'gh-r' nocompile sbin'jless' for \
  PaulJuliusMartinez/jless

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)jaq*]} ]]' \
  from'gh-r' bpick"*-x86_64-unknown-linux-gnu" nocompile sbin'*->jaq' for \
  @01mf02/jaq

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)ba*]} ]]' \
  from'gh-r' nocompile sbin'bandwhich' for \
  imsnif/bandwhich

zi ice wait'[[ -n ${ZLAST_COMMANDS[(r)ps*]} ]]' lucid as"program" mv"ps_mem.py -> psmem" pick"psmem"
zi light "pixelb/ps_mem"

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)yt*]} ]]' \
  from'gh-r' bpick"yt-dlp" nocompile sbin'yt-dlp->yt' for \
  yt-dlp/yt-dlp

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)fd*]} ]]' \
  from"gh-r" nocompile sbin"fd" \
  atclone"cp -vf **/fd.1 $ZINIT[MAN_DIR]/man1" for \
 @sharkdp/fd

# zi lucid light-mode \
#   from'gh-r' bpick"*linux-gnu*" nocompile sbin'dust' for \
#   "bootandy/dust"

# zi lucid light-mode \
#   from'gh-r' bpick"*linux*" nocompile sbin'zellij' for \
#   "@zellij-org/zellij"

# zi ice lucid as"program" pick"pfetch"
# zi light "dylanaraps/pfetch"

# zi ice lucid from"gh-r" as"program" bpick"*linux*"
# zi light "casey/intermodal"

# zi ice lucid from"gh-r" as"program" bpick"*linux-x86_64*" mv"shell/exercism_completion.zsh -> _exercism"
# zi light "exercism/cli"

zi lucid light-mode from'gh-r' nocompile wait'[[ -n ${ZLAST_COMMANDS[(r)tm*]} ]]' \
   atclone'gzip -d */man/tmsu.1.gz; cp -vf **/*.1 $ZINIT[MAN_DIR]/man1' \
   sbin'tmsu' for \
   @oniony/TMSU

# zi lucid light-mode from'gh-r' nocompile atclone'mv par2cmdline-turbo-v* par2' sbin'par2' for \
#   @animetosho/par2cmdline-turbo

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)ja*]} ]]' from'gh-r' bpick"*LinuxAMDx64*" nocompile sbin'jackett' for \
  @Jackett/Jackett

# zi lucid light-mode \
#   from'gh-r' sbin'texlab' for \
#   "latex-lsp/texlab"

# zi lucid light-mode from"gh-r" nocompile sbin"ouch" for \
#  @ouch-org/ouch

# zi lucid light-mode \
#   from'gh-r' fbin'!' \
#   atclone"cp -vf **/*.1 $ZINIT[MAN_DIR]/man1; mv ./**/_ouch _ouch" for \
#   "ouch-org/ouch"

zi lucid light-mode from'gh-r' bpick"*linux*" nocompile wait'[[ -n ${ZLAST_COMMANDS[(r)hl*]} ]]' \
  atclone'tar xvf hledger-linux-x64.tar; rm hledger-linux-x64.tar' atpull"%atclone" sbin'hledger' for \
  @simonmichael/hledger

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)hx*]} ]]' from'gh-r' nocompile atclone"mv */contrib/completion/hx.zsh _hx" sbin'hx' for \
  @helix-editor/helix

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)zed*]} ]]' from'gh-r' nocompile sbin'zed' for \
  @zed-industries/zed

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)kitt*]} ]]' from'gh-r' bpick"kitty*x86_64*" nocompile sbin'bin/*' for \
  @kovidgoyal/kitty

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)stream*]} ]]' from'gh-r' nocompile sbin'*->streamlink' for \
  @streamlink/streamlink-appimage

zi lucid light-mode wait'[[ -n ${ZLAST_COMMANDS[(r)freet*]} ]]' from'gh-r' bpick"*.AppImage" nocompile sbin'*->freetube' for \
  @FreeTubeApp/FreeTube
