// user_pref("mousewheel.system_scroll_override.enabled", true);
// user_pref("apz.gtk.kinetic_scroll.enabled", true);
// user_pref("general.smoothScroll.mouseWheel.durationMinMS", 50);
user_pref("mousewheel.default.delta_multiplier_y", 50);
// user_pref("apz.overscroll.enabled", true); // Scroll bounce
// user_pref("widget.disable-swipe-tracker", false);

user_pref("widget.non-native-theme.scrollbar.style", 3);
user_pref("ui.scrollbarDisplayOnMouseMove", 0);

user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", false);
user_pref("browser.compactmode.show", false);
// user_pref("layout.css.backdrop-filter.enabled", true);

user_pref("browser.sessionstore.restore_pinned_tabs_on_demand", true);
user_pref("browser.tabs.closeWindowWithLastTab", false);

// user_pref("network.http.referer.XOriginPolicy", 0);

// user_pref("gfx.downloadable_fonts.enabled ", false);

user_pref("accessibility.force_disabled", 1);
// user_pref("accessibility.typeaheadfind", false);
user_pref("accessibility.typeaheadfind.enablesound", false);

// DNS
// user_pref("network.dns.disablePrefetchFromHTTPS", false); // Prefetch DNS on HTTPS sites
user_pref("network.predictor.enable-prefetch", true);

// user_pref("network.dns.echconfig.enabled", true);

// user_pref("network.http.http3.cc_algorithm", 1);

user_pref("widget.gtk.ignore-bogus-leave-notify", 1); // to drag tabs in TST

// user_pref("browser.translations.enable", true);

// user_pref("dom.text_fragments.enabled", true);

// WebGPU
user_pref("dom.webgpu.enabled", true);
// user_pref("gfx.webrender.all", true);
