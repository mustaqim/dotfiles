(setq gc-cons-threshold most-positive-fixnum)
(add-hook 'emacs-startup-hook (lambda ()
  (setq gc-cons-threshold (* 2 1024 1024))))
